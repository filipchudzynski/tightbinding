import numpy as np
import os
import pandas as pd 
import sklearn.neighbors as sn

#where a is distance between atoms
def hexagonSide(side,a,x,h=5):
    if side == "LeftDown":
        return -np.sqrt(3) * x + np.sqrt(3)/2 * a + h
    elif side == "RightDown": 
        return np.sqrt(3) * (-2*a+x) + np.sqrt(3)*a/2 + h 
    elif side == "LeftUp":
        return np.sqrt(3) * x + np.sqrt(3)/2 * a + h
    elif side == "RightUp":
        return -np.sqrt(3) * (-2*a+x) + np.sqrt(3)*a/2 + h 
    elif side == "Top":
        return np.sqrt(3) * a + h
    elif side == "Bottom":
        return 0 + h

def diskSide(side,r,x,h=0):
    if side == "Top":
        return np.sqrt(a**2-(x-a)**2) + a + h
    elif side == "Bottom":
        return -np.sqrt(a**2-(x-a)**2) + a + h


#https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres
#generated according to the algorithm in wikipedia
#position of atoms are described with vector
# |2i + (j+k) % 2                 |
# |np.sqrt(3) * (j + 1/3*k % 2)   | * r
# |2* np.sqrt(6) / 3 * k          |
# where i,j,k are integers and r is half of the distance between neighbouring atoms


#popraw algorytm
#tylko 3 warstwy są unikatowe
#wygeneruj 3 pierwsze warstwy
#sprawdź ile wielokrotności 3 warstw mieści się w całym NW
#zrób n kopii tych 3 warstw a następnie dołóż brakujące
#dopisz z-wą składową tak żeby się zgadzało
class Layer:
    Elements=["In","P","As"]
    def __init__(self,atomPoss=None,element="In",NOfAtoms=0):
        if atomPoss is not None:
            self.atomPoss = atomPoss
        else:
            self.atomPoss = np.array([])
        if element in self.Elements:
            self.element = element
        self.NOfAtoms=NOfAtoms
       

class Crystal:
    def __init__(self,semiconductors):
        self.semiconductors = semiconductors
        self.atomListToPrint = []
        self.atomList = np.empty((0,3))
        self.semiconductorList = []
        self.elementsList = []
        self.numberOfAtoms = 0
        firstLayer = "A"
        for semiconductor in semiconductors:
            semiconductor.firstLayer=firstLayer
            semiconductor.extrude_crystal()
            semiconductor.generate_ABC_layers()
            semiconductor.calculateNumberOfAtoms()
            firstLayer=semiconductor.getLayerAfterFinalLayer()
            print(semiconductor.layerSequence)
        

    def outputAtomPositions(self):
        offset = self.semiconductors[0].offset
        firstLayer = self.semiconductors[0].firstLayer
        for semiconductor in self.semiconductors:
            semiconductor.offset = offset
            semiconductor.firstLayer = firstLayer
            semiconductorAtomList, atomList,elementsList, numberOfAtoms = semiconductor.outputAtomPositions()
            self.numberOfAtoms+=numberOfAtoms
            self.atomListToPrint+=semiconductorAtomList
            self.atomList=np.vstack((self.atomList,atomList))
            self.semiconductorList+=[semiconductor.semiconductor]*numberOfAtoms
            self.elementsList+=elementsList
            offset += semiconductor.height
            firstLayer = semiconductor.getLayerAfterFinalLayer()
        self.atomList = np.array(self.atomList)
        outputString = "{}\n as in\n".format(self.numberOfAtoms)
        outputString += "\n".join(self.atomListToPrint)
        path='/home/filip/Desktop/'
        name='slab_structure_atom_list'
        file = open(os.path.join(path,name) + '.xyz', 'w')
        file.write(outputString)
        file.close()
        print("atom list saved in {}".format(os.path.join(path,name) + '.xyz'))

class Semiconductor: 
    shapes = {"square":1,"circle":2,"hexagon":3}
    layerNames = ["A","B","C"]
    baseNames = ["A","B"]
    def __init__(self,x_lim,y_lim,z_lim,latticeConst,shape=None,semiconductor=("In","As"),offset=0):
        self.x_lim, self.y_lim, self.z_lim = x_lim, y_lim, z_lim
        baseAlayers = {"A":Layer(),"B":Layer(),"C":Layer()}
        baseBlayers = {"A":Layer(),"B":Layer(),"C":Layer()}   
        self.semiconductor = semiconductor
        self.atoms = {"A":baseAlayers,"B":baseBlayers}
        self.latticeConst = latticeConst
        self.interAtomDist = latticeConst/(2*np.sqrt(2))
        self.baseTranslationVect = np.array([1,1/np.sqrt(3),np.sqrt(6)/6] )*self.interAtomDist 
        if shape in self.shapes:
            self.shape = self.shapes[shape]
        else:
            self.shape = self.shapes["square"] 
        
        self.layerSequence = []
        self.NOfAtoms=0
        self.NOfAtomsInABCBlock = 0
        self.z_const = 2*np.sqrt(6)/3 
        self.offset = offset
        self.height = 0
        self.firstLayer = "A"
        self.indexOffset = 0
        self.layerOffset = 0

    def generate_ABC_layers(self,start_vector=np.array([0,0,0])):
        print("generating ABC layers")
        x_lim, y_lim = self.x_lim, self.y_lim
        atomA, atomB = self.semiconductor
        #redefine limits in order to have integer limits
        a = self.interAtomDist
        for k in range(0,3):
            tempLayerAtomA = []
            tempLayerAtomB = []
            for j in range(int(y_lim)):
                for i in range(int(x_lim)):
                        if (j + k%3/3)*np.sqrt(3)*a >= hexagonSide("LeftDown",x_lim/2,(2*i + (j+k%3)%2)*a) and (j + k%3/3)*np.sqrt(3)*a >= hexagonSide("RightDown",x_lim/2,(2*i + (j+k%3)%2)*a) :  
                            if (j + k%3/3)*np.sqrt(3)*a <= hexagonSide("LeftUp",x_lim/2,(2*i + (j+k%3)%2)*a)  and (j + k%3/3)*np.sqrt(3)*a <= hexagonSide("RightUp",x_lim/2,(2*i + (j+k%3)%2)*a) : 
                                if (j + k%3/3)*np.sqrt(3)*a >= hexagonSide("Bottom",x_lim/2,(2*i + (j+k%3)%2)*a) and (j + k%3/3)*np.sqrt(3)*a <= hexagonSide("Top",x_lim/2,(2*i + (j+k%3)%2)*a) :
                                    # print( np.array( [2 * i + (j + k) % 2, np.sqrt(3) * (j + k%2/3) , k * z_const ] )*a )
                                    tempVect = np.array( [2 * i + (j + k%3) % 2, np.sqrt(3) * (j + k%3/3) , (k+self.layerOffset)%3 * self.z_const ] )*a 
                                    tempLayerAtomA.append(                           tempVect ) 
                                    tempLayerAtomB.append(self.baseTranslationVect + tempVect )
                                    
            self.atoms["A"][self.layerNames[k]].atomPoss = np.array(tempLayerAtomA)
            self.atoms["A"][self.layerNames[k]].element = atomA
            self.atoms["A"][self.layerNames[k]].NOfAtoms = len(tempLayerAtomA)

            self.atoms["B"][self.layerNames[k]].atomPoss = np.array(tempLayerAtomB)
            self.atoms["B"][self.layerNames[k]].element = atomB
            self.atoms["B"][self.layerNames[k]].NOfAtoms = len(tempLayerAtomB)
            self.NOfAtomsInABCBlock += self.atoms["A"][self.layerNames[k]].NOfAtoms 
            self.NOfAtomsInABCBlock += self.atoms["B"][self.layerNames[k]].NOfAtoms
            
    def extrude_crystal(self):
        limit_z=self.z_lim
        print("extruding crystal")
        #why substracting one layer!?
        limit_z /= self.z_const*self.interAtomDist
        self.height = int(limit_z)*self.z_const*self.interAtomDist
        nOfABCBlocks = int(limit_z/3)
        rest = int(limit_z%3)
        while self.layerNames[0] is not self.firstLayer:
            self.layerOffset+=1
            self.layerNames=[self.layerNames[-1]] + self.layerNames[:-1]
        restLayers = self.layerNames[:rest]
        #layerSequence=layerSequence + restLayersA
        self.layerSequence = self.layerNames * nOfABCBlocks + restLayers

        # self.layerNames = ["A","B","C"]
        
    def calculateNumberOfAtoms(self):
        self.NOfAtoms += int(len(self.layerSequence)/3) * self.NOfAtomsInABCBlock
        print(self.NOfAtoms)
        for r in self.layerNames[:int(len(self.layerSequence)%3)]:
            self.NOfAtoms+=self.atoms["A"][r].NOfAtoms
            self.NOfAtoms+=self.atoms["B"][r].NOfAtoms
        print(self.NOfAtoms)
        
    def getLayerAfterFinalLayer(self):
        layers = sorted(self.layerNames)
        return layers[(layers.index(self.layerSequence[-1])+1)%3]

    # def reorderLayers(self):
    #     if self.firstLayer!=self.layerSequence[0]:
    #         layers = sorted(self.layerNames)
    #         firstLayerIndex=layers.index(self.firstLayer)
    #         layersToPrepend = layers[firstLayerIndex:]
    #         self.indexOffset = len(layersToPrepend)
    #         self.layerSequence = layersToPrepend + self.layerSequence
    #         for x in layersToPrepend:
    #             self.layerSequence.pop()
            

    def outputAtomPositions(self):
        print("listing position of atoms")
        atomListToPrint= []
        atomList = np.empty((0,3))
        numberOfLayers = len(self.layerSequence)
        elementsList = []
        # self.reorderLayers()
        print(self.layerSequence)
        for b in self.baseNames:
            for index,l in enumerate(self.layerSequence):
                print("semiconductor:{} layer {} {}/{} of atoms from base {} ".format(self.semiconductor,l,index,numberOfLayers,b))
                layer = self.atoms[b][l]
                z_block_const = self.z_const*3*self.interAtomDist
                z_translation = self.offset + int(index/3) * z_block_const
                layerAtoms = layer.atomPoss + np.array([0,0,z_translation])
                atomList=np.vstack((atomList,layerAtoms))
                element = layer.element
                elementsList+=[element]*len(layerAtoms)
                for atoms in layerAtoms:
                    atomListToPrint.append("{} {:8.5f} {:8.5f} {:8.5f}".format(element, atoms[0],atoms[1],atoms[2]))
        return atomListToPrint, atomList, elementsList, self.NOfAtoms
       

def generate__hcp_structure(x_lim,y_lim,z_lim,a,start_vector=np.array([0,0,0])):
    z_const = 2*np.sqrt(6)/3 
    z_lim /= z_const * a
    y_lim /= np.sqrt(3) * a
    k = 0
    structure = []
    while k  <= z_lim:
        j = 0
        while j + k%2/3 <= y_lim:
            i = 0
            while 2*i + (j+k)%2 <= x_lim / a:
                # print( np.array( [2 * i + (j + k) % 2, np.sqrt(3) * (j + k%2/3) , k * z_const ] )*a )
                structure.append( start_vector + np.array( [2 * i + (j + k) % 2, np.sqrt(3) * (j + k%2/3) , k * z_const ] )*a )
                i+=1
            j+=1
        k+=1

    return structure    
def generate_carbon_nanowire(a,dimensions,shape,semiconductor):
    C = Semiconductor(dimensions[0],dimensions[1],dimensions[2],latticeConst=a,shape=shape,semiconductor=semiconductor)
    nanowire = Crystal([C])
    nanowire.outputAtomPositions()
    size = nanowire.numberOfAtoms
    final_lattice = pd.DataFrame({'number_of_atom': np.arange(0, size, 1), 'localization': list(nanowire.atomList),
                                      'type_of_atom': nanowire.elementsList,'semiconductor': nanowire.semiconductorList})
    return nanowire.atomList, final_lattice, nanowire.numberOfAtoms

# ,lattice,_ =generate_carbon_nanowire()

# print(lattice[100:150])
# a = 3.567


# points,_,_ = generate_carbon_nanowire(5.8687,())
# distance =1.54455
# print(distance)
# tree = sn.KDTree(np.array(points), leaf_size=2)
# close_friends, nn_dist= tree.query_radius(points, r=distance + 1, sort_results=True, return_distance=True)
# for friends in close_friends:
#     if len(friends)<5:
#         print(points[friends[0]])

#85nm width InP 1.5 um InAs 5nm InP 0.5 um
# a = 5.8687#InP
# InP = Semiconductor(85,85,100,latticeConst=a,shape="hexagon",semiconductor=("In","P"))
# InAs = Semiconductor(85,85,10,latticeConst=a,shape="hexagon",semiconductor=("In","As"))
# InP2 = Semiconductor(85,85,90,latticeConst=a,shape="hexagon",semiconductor=("In","P"))


# nanowire = Crystal([InP,InAs,InP2])
# nanowire.outputAtomPositions()37
# print(nanowire.atomList)

# #distance between atoms from one basis to (4)atoms from different base 
# closest_neighbour_dist = np.sqrt(3*(1/4)**2)*a
# print(closest_neighbour_dist)
##policz węglowy nanodrut
##10at w warstwie * 12

##jeśli zadziała to zrób dwuatomową bazę
##
