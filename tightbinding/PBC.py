import numpy as np

positions = np.array([[0,0,0],
                [5,0,0],[0,5,0],[0,0,5],
                [5,5,0],[5,0,5],[0,5,5],
                [5,5,5],
                ])
atoms = dict()
for i,a in enumerate(positions):
    atoms[i] = []
    atoms[i].append(a)



dimensions= np.array([5,5,5])



unit_vecs = np.array([  [0,0,0],
                        [1,0,0],[0,1,0],[0,0,1],[-1,0,0],[0,-1,0],[0,0,-1],
                        [1,1,0],[-1,1,0],[1,-1,0],[-1,-1,0],
                        [1,0,1],[-1,0,1],[1,0,-1],[-1,0,-1],
                        [0,1,1],[0,-1,1],[0,1,-1],[0,-1,-1],
                        [1,1,1],[-1,1,1],[1,-1,1],[1,1,-1],[-1,-1,1],[1,-1,-1],[-1,1,-1],[-1,-1,-1]])

translations = dimensions*unit_vecs

PBC_lattice=[a for a in positions]


print([6.25, 6.25, 6.25] in positions)
ind = 0
for t in translations:
        tvec = t+positions
        for i, v in enumerate(tvec):
            if list(v) not in positions.tolist():
                PBC_lattice.append(v)
                atoms[i].append(v)
# print([6.25, 6.25, 6.25] in vec)

printPBC=[]
print(PBC_lattice)
for v in PBC_lattice:

#             for v in supv:
    printPBC.append("{} {:8.5f} {:8.5f} {:8.5f}".format("Si", v[0],v[1],v[2]))

outputString = "{}\n as in\n".format(len(printPBC))
outputString += "\n".join(printPBC)

print(outputString)