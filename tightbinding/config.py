import numpy as np

def V_alpha(h, r=1.42):
  n = 2.
  n_c = 6.5
  r_o = 1.536329
  r_c = 2.18
  s = np.exp((-n*(r/r_c)**(n_c) + n*(r/r_c)**(n_c))) * (r_o/r)**n 
  h_alpha = h * s
  return h_alpha

def distanceToAtom(a):
    return np.sqrt(12)*a/8

diagonal_energies1 = {('C', 'C'):
                                {'C': 
                                    {'Es': -50,
                                    'Epx': -50,
                                    'Epy': -50,
                                    'Epz': 0,
                                    'Edz2': 4000.,
                                    'Edxz': 4000.,
                                    'Edyz': 4000.,
                                    'Edxy': 4000.,
                                    'Edx2y2': 4000.,
                                    'Estar': 4000.
                                    }
                                }
                    }   

                              
interactive_constans1 = {('C', 'C'):
                                    {('C', 'C'):
                                                {'V_SS_sigma': 0,
                                                'V_spsigma': 0,
                                                'V_sdsigma': 0.,
                                                'V_starsssigma':0,
                                                'V_starssigma': 0.,
                                                'V_starpsigma': 0.,
                                                'V_stardsigma': 0.,
                                                'V_PP_sigma': 0,
                                                'V_PP_pi': -2.6,
                                                'V_pdsigma': 0.,
                                                'V_pdpi': 0.,
                                                'V_DD_sigma': 0.,
                                                'V_DD_pi': 0.,
                                                'V_DD_delta': 0.
                                                }
                                    }
                        } 


diagonal_energies2 = {('C', 'C'):
                                {'C': 
                                    {'Es': -2.990,
                                    'Epx': 3.710,
                                    'Epy': 3.710,
                                    'Epz': 3.710,
                                    'Edz2': 4000.,
                                    'Edxz': 4000.,
                                    'Edyz': 4000.,
                                    'Edxy': 4000.,
                                    'Edx2y2': 4000.,
                                    'Estar': 4000.
                                    }
                                } 
                    }  
                                                    

interactive_constans2 = {('C', 'C'):
                                    {('C', 'C'):
                                                {'V_SS_sigma': -5.000,
                                                'V_spsigma': 4.700,
                                                'V_sdsigma': 0.,
                                                'V_starsssigma':0,
                                                'V_starssigma': 0.,
                                                'V_starpsigma': 0.,
                                                'V_stardsigma': 0.,
                                                'V_PP_sigma': 5.500,
                                                'V_PP_pi': -1.550,
                                                'V_pdsigma': 0.,
                                                'V_pdpi': 0.,
                                                'V_DD_sigma': 0.,
                                                'V_DD_pi': 0.,
                                                'V_DD_delta': 0.
                                                }
                                     }
                        }    


diagonal_energies3 = {('C', 'C'):
                                {'C': 
                                    {'Es': -10.290,
                                    'Epx': 0,
                                    'Epy': 0,
                                    'Epz': 0,
                                    'Edz2': 4000.,
                                    'Edxz': 4000.,
                                    'Edyz': 4000.,
                                    'Edxy': 4000.,
                                    'Edx2y2': 4000.,
                                    'Estar': 4000.
                                    }
                                }  
                    }    
                                           

interactive_constans3 = {('C', 'C'):
                                    {('C', 'C'): 
                                                {'V_SS_sigma': -8.42256,
                                                'V_spsigma': 8.08162,
                                                'V_sdsigma': 0.,
                                                'V_starsssigma':0,
                                                'V_starssigma': 0.,
                                                'V_starpsigma': 0.,
                                                'V_stardsigma': 0.,
                                                'V_PP_sigma': 7.75792,
                                                'V_PP_pi': -3.67510,
                                                'V_pdsigma': 0.,
                                                'V_pdpi': 0.,
                                                'V_DD_sigma': 0.,
                                                'V_DD_pi': 0.,
                                                'V_DD_delta': 0.
                                                }
                                    }
                        } 


diagonal_energies4 = {('C', 'C'):
                                {'C':
                                     {'Es': -2.99,
                                    'Epx': 3.71,
                                    'Epy': 3.71,
                                    'Epz': 3.71,
                                    'Edz2': 4000.,
                                    'Edxz': 4000.,
                                    'Edyz': 4000.,
                                    'Edxy': 4000.,
                                    'Edx2y2': 4000.,
                                    'Estar': 4000.
                                    }
                                }
                        }   
                                           

interactive_constans4 = {('C', 'C'):
                                    {('C', 'C'): 
                                                {'V_SS_sigma': V_alpha(h=-5., r=1.42),
                                                'V_spsigma': V_alpha(h=4.7, r=1.42),
                                                'V_sdsigma': 0.,
                                                'V_starsssigma':0,
                                                'V_starssigma': 0.,
                                                'V_starpsigma': 0.,
                                                'V_stardsigma': 0.,
                                                'V_PP_sigma': V_alpha(h=5.5, r=1.42),
                                                'V_PP_pi': V_alpha(h=-1.55, r=1.42),
                                                'V_pdsigma': 0.,
                                                'V_pdpi': 0.,
                                                'V_DD_sigma': 0.,
                                                'V_DD_pi': 0.,
                                                'V_DD_delta': 0.
                                                }
                                    }
                        }    




diagonal_energies5 = {('C', 'C'):
                                {'C':
                                    {'Es': -1.0458,
                                    'Epx': 7.0850,
                                    'Epy': 7.0850,
                                    'Epz': 7.0850,
                                    'Edz2': 27.9267,
                                    'Edxz': 27.9267,
                                    'Edyz': 27.9267,
                                    'Edxy': 27.9267,
                                    'Edx2y2': 27.9267,
                                    'Estar': 38.2661
                                    }
                                }
                    }   
                                           

interactive_constans5 = {('C', 'C'):
                                    {('C', 'C'):
                                                {'V_SS_sigma': V_alpha(h=-4.3882, r= 1.54),
                                                'V_spsigma': V_alpha(h=5.4951, r= 1.54),
                                                'V_sdsigma': 0,#V_alpha(h=-2.7655, r= 1.54),
                                                'V_starsssigma':V_alpha(h=-2.3899, r= 1.54),
                                                'V_starssigma': V_alpha(h=-2.6737, r= 1.54),
                                                'V_starpsigma': V_alpha(h=5.1709, r= 1.54),
                                                'V_stardsigma': 0,#V_alpha(h=-2.3034, r= 1.54),
                                                'V_PP_sigma': V_alpha(h=7.5480, r= 1.54),
                                                'V_PP_pi': V_alpha(h=-2.6363, r= 1.54),
                                                'V_pdsigma': 0,#V_alpha(h=-2.1621, r= 1.54),
                                                'V_pdpi': 0,#V_alpha(h=3.9281, r= 1.54),
                                                'V_DD_sigma': V_alpha(h=-4.1813, r= 1.54),
                                                'V_DD_pi': V_alpha(h=4.9779, r= 1.54),
                                                'V_DD_delta': V_alpha(h=-3.9884, r= 1.54)
                                                }
                                                
                                    }
                        }


interactive_constans_spin_spin_orbita = {'C': {'Es up up': 1,
                                               'Epx up up': 1.,
                                               'Epy up up': 1,
                                               'Epz up up': 1,
                                               'Edz2 up up': 1,
                                               'Edxz up up': 1,
                                               'Edyz up up': 1,
                                               'Edxy up up': 1,
                                               'Edx2y2 up up': 1,
                                               'Estar up up': 1,
                                               'Es down down': 1,
                                               'Epx down down': 1,
                                               'Epy down down': 1,
                                               'Epz down down': 1,
                                               'Edz2 down down': 1,
                                               'Edxz down down': 1,
                                               'Edyz down down': 1,
                                               'Edxy down down': 1,
                                               'Edx2y2 down down': 1,
                                               'Estar down down': 1}}

###################### DATA FOR NANOWIRES CALCULATION ######################
######################                                ######################

#Data from Nearest-neighbor sp 3 d 5 s ∗ tight-binding
# parameters based on the hybrid quasi-particle
# self-consistent GW method verified by modeling
# of type-II superlattices 
# 1 Jun 2018

diagonal_energies_compounds={('In','P'):
                                    {"In":
                                        {
                                        'Es': -4.9766,
                                        'Epx': 2.7952,
                                        'Epy': 2.7952,
                                        'Epz': 2.7952,
                                        'Edz2': 13.8323,
                                        'Edxz': 13.8323,
                                        'Edyz': 13.8323,
                                        'Edxy': 13.8323,
                                        'Edx2y2': 13.8323,
                                        'Estar': 20.1936},
                                    "P":{
                                        'Es': 0.1068,
                                        'Epx': 5.9043,
                                        'Epy': 5.9043,
                                        'Epz': 5.9043,
                                        'Edz2': 12.9168,
                                        'Edxz': 12.9168,
                                        'Edyz': 12.9168,
                                        'Edxy': 12.9168,
                                        'Edx2y2': 12.9168,
                                        'Estar': 18.7500}   
                                    },
                             ('In','As'):
                                    {"In":{
                                        'Es': -5.6648,
                                        'Epx': 3.6412,
                                        'Epy': 3.6412,
                                        'Epz': 3.6412,
                                        'Edz2': 12.8510,
                                        'Edxz': 12.8510,
                                        'Edyz': 12.8510,
                                        'Edxy': 12.8510,
                                        'Edx2y2': 12.8510,
                                        'Estar': 18.2846},
                                    "As":{
                                        'Es': -0.2583,
                                        'Epx': 5.4428,
                                        'Epy': 5.4428,
                                        'Epz': 5.4428,
                                        'Edz2': 13.4309,
                                        'Edxz': 13.4309,
                                        'Edyz': 13.4309,
                                        'Edxy': 13.4309,
                                        'Edx2y2': 13.4309,
                                        'Estar': 17.5940}   
                                    }
                              }




interactive_constants_compounds={('In','P'):
                                        {('In','P'):
                                                    {
                                                    'V_SS_sigma': -1.0953,
                                                    'V_S*S*_sigma': -4.9283,
                                                    'V_S*aSc_sigma':-0.9165,
                                                    'V_SaS*c_sigma':-2.9356,
                                                    'V_SaPc_sigma':3.0329,
                                                    'V_ScPa_sigma':1.9681,
                                                    'V_S*aPc_sigma':1.1163,
                                                    'V_S*cPa_sigma':0.9064,
                                                    'V_SaDc_sigma':-3.6861,
                                                    'V_ScDa_sigma':-3.0201,
                                                    'V_S*aDc_sigma':-0.7530,
                                                    'V_S*cDa_sigma':-2.4643,
                                                
                                                    'V_PP_sigma': 3.9017,
                                                    'V_PP_pi': -0.9863,
                                                    'V_PaDc_sigma':-0.9146,
                                                    'V_PcDa_sigma':-2.7718,
                                                    'V_PaDc_pi':0.8296,
                                                    'V_PcDa_pi':1.3465,
                                                    
                                                    'V_DD_sigma':-1.3804,
                                                    'V_DD_pi':2.9123,
                                                    'V_DD_delta':-2.2435,
                                                    'V_DELTA_a/3':0.0434,
                                                    'V_DELTA_c/3':0.1433
                                                    }
                                        },
                                 ('In','As'):
                                            {('In','As'):
                                                        {
                                                        'V_SS_sigma': -1.0424,
                                                        'V_S*S*_sigma': -2.2986,
                                                        'V_S*aSc_sigma': -1.1236,
                                                        'V_SaS*c_sigma': -3.1810,
                                                        'V_SaPc_sigma': 2.9291,
                                                        'V_ScPa_sigma': 1.8408,
                                                        'V_S*aPc_sigma': 2.9935,
                                                        'V_S*cPa_sigma': 1.2504,
                                                        'V_SaDc_sigma': -3.7194,
                                                        'V_ScDa_sigma': -3.2657,
                                                        'V_S*aDc_sigma': -1.2525,
                                                        'V_S*cDa_sigma': -1.1698,
                                                    
                                                        'V_PP_sigma': 3.8430,
                                                        'V_PP_pi': -0.9976,
                                                        'V_PaDc_sigma': -1.6600,
                                                        'V_PcDa_sigma': -3.6682,
                                                        'V_PaDc_pi': 0.9864,
                                                        'V_PcDa_pi': 0.7884,
                                                        
                                                        'V_DD_sigma': -0.5417,
                                                        'V_DD_pi': 3.4663,
                                                        'V_DD_delta': -2.3433,
                                                        'V_DELTA_a/3': 0.1862,
                                                        'V_DELTA_c/3': 0.1580
                                                        }
                                            },       
                                  }

configuration = {

'parametrization1':
{'lattice_type': 'rectagonal1_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 120,
 'horizontal_num_of_steps': 8,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies1,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 3600, #4910
 'interactive_constans': interactive_constans1,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

 'parametrization2':
{'lattice_type': 'rectagonal2_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.536329, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 120,
 'horizontal_num_of_steps': 8,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies2,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 3600, #4910
 'interactive_constans': interactive_constans2,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

 'parametrization3':
{'lattice_type': 'rectagonal3_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.312, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 120,
 'horizontal_num_of_steps': 8,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies3,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 3600, #4910
 'interactive_constans': interactive_constans3,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

 'parametrization4':
{'lattice_type': 'rectagonal4_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1., # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies1,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 3600, #4910
 'interactive_constans': interactive_constans1,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

 'parametrization5':
{'lattice_type': 'rectagonal5_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.536329, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies2,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 3600, #4910
 'interactive_constans': interactive_constans2,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

 'parametrization6':
{'lattice_type': 'rectagonal6_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.312, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies3,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 4000, #4910
 'interactive_constans': interactive_constans3,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

 'parametrization7':
{'lattice_type': 'rectagonal7_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies4,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 4000, #4910
 'interactive_constans': interactive_constans4,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},


 'parametrization8':
{'lattice_type': 'rectagonal8_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 120,
 'horizontal_num_of_steps': 8,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies4,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 3600, #4910
 'interactive_constans': interactive_constans4,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},


'parametrization9':
{'lattice_type': 'rectagonal9',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies5,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 4000, #4910
 'interactive_constans': interactive_constans5,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},


 'parametrization10':
{'lattice_type': 'rectagonal10',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 120,
 'horizontal_num_of_steps': 8,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies5,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 4000, #4910
 'interactive_constans': interactive_constans5,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},


 'parametrization11':
{'lattice_type': 'rectagonal11_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies5,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 4000, #4910
 'interactive_constans': interactive_constans5,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': True,
 'number_of_defects': None},


 'parametrization12':
{'lattice_type': 'rectagonal12_40_proc_of_defects',
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': 1.42, # 1.536329 pomyśleć z tymi zmiennoprzecinkowymi 
 'lanczos_vectors':None,
 'vertical_num_of_steps': 120,
 'horizontal_num_of_steps': 8,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies5,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 4000, #4910
 'interactive_constans': interactive_constans5,
 'saving_directory': '/home/przemek/Documents/Modeling/tight_binding/results_diploma',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': True,
 'number_of_defects': None},

'parametrizationInPNW':
{'lattice_type': 'nanowire',
 'shape': "hexagon",
 'lattice_constant': 5.8687,
 'dimensions': (15,15,10),
 'semiconductor': ("In","P"),
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': distanceToAtom(5.8687), #d = a * sqrt(2)/2 - distance between atoms of the same type. Radius of a exsphere is d*sqrt(6)/4. In total a *sqrt(12)/8  
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies_compounds,# diagonal_energies_compounds,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 159,
 'interactive_constans': interactive_constants_compounds, #interactive_constants_compounds
 'saving_directory': '/home/filip/Desktop/Single_Photon_Emiters/TB/results',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},

'parametrizationInAsNW':
{'lattice_type': 'nanowire',
 'shape': "hexagon",
 'lattice_constant': 6.0583,
 'dimensions': (15,15,10),
 'semiconductor': ("In","P"),
 'gauss_sigma': 0.015,
 'start': -17,
 'stop': 17,  
 'step': 0.001,
 'distance': distanceToAtom(6.0583), #d = a * sqrt(2)/2 - distance between atoms of the same type. Radius of a exsphere is d*sqrt(6)/4. In total a *sqrt(12)/8  
 'lanczos_vectors':None,
 'vertical_num_of_steps': 20,
 'horizontal_num_of_steps': 60,
 'x_num_of_steps': 39,
 'sigma': 0.0001, 
 'magnitude': 'LM',
 'neighbour_calculation_method': 'distance',
 'diagonal_energies':diagonal_energies_compounds,# diagonal_energies_compounds,
 'calculation_type': 'non spin',
 'number_of_eigenvalues': 139,
 'interactive_constans': interactive_constants_compounds, #interactive_constants_compounds
 'saving_directory': '/home/filip/Desktop/Single_Photon_Emiters/TB/results',
 'ld':None,
 'lp': None,
 'number_of_friends':None,
 'defects': None,
 'number_of_defects': None},
} 


# TODO policzyc uklady z parametryzacją publikacje 
# TODO poiczyć dla dewastacji na brzegach heksagonalnej struktury i prostokątnej
#sys.argv[1]